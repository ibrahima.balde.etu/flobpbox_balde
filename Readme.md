## Projet 1 - Systèmes Répartis 2 : API Flopbox

## Auteur

- Ibrahima BALDE

## Encadrant

- Arthur d'Azemar

25/03/2021

## Introduction

Développement d'une plate-forme FlopBox en adoptant le style architectural REST pour permettre de centraliser la gestion de fichiers distants stockés dans des serveurs FTP tiers.

Le mécanisme d'authentification utilisé est les tokens grâce à la librarie jwt.

Pour stocker les comptes utlisateurs dans la plateforme, mongodb a été utilisé.

Le code a été fait en nodejs

## Lancer le programme

    Récupérer le projet par: $ git pull

    Se placer au dossier racine du projet

    Créér un dossier data à la racine du projet s'il n'existe pas, ensuite taper la commande : mongod --dbpath data

    Ensuite executer la commande : npm start

    Note : L'application devait être dockeriser pour plus de simplicité

## Liste des requetes avec curl

J'ai testé mes routes avec les données qui sont là

### Inscription d'un client dans la plateforme flopbox

```bash

 curl -X POST -H 'Accept: application/json' -H 'Content-Type: application/json' -i http://localhost:3000/users/subscribe --data '{"name": "balde", "password":"balde"}'
```

### Connection d'un client dans la plateforme flopbox

```bash
curl -X POST -H 'Accept: application/json' -H 'Content-Type: application/json' -i http://localhost:3000/users/connect --data '{"name": "ibrahim", "password":"balde"}'
```

Pour le serveur python avec le lancer avec le port 2121

```bash
curl -X GET -H 'Content-Type: application/json' -H 'Accept: application/json' -i http://localhost:3000/server/connect/python
```

### Ajouter un server dans la plateforme flopbox

```bash
curl -X POST -H 'Accept: application/json' -H 'Content-Type: application/json' -i http://localhost:3000/server/add --data '{"alias": "ubuntu", "host":"ftp.ubuntu.com", "user": "anonymous", "password":"anonymous", "port": 21}'
```

### Se connecter a un server avec alias dans la plateforme flopbox

```bash
curl -X GET -H 'Accept: application/json' -H 'Content-Type: application/json' -i http://localhost:3000/server/connect/ubuntu
```

### Supprimer un server dans la plateforme flopbox

```bash
curl -X GET -H 'Accept: application/json' -H 'Content-Type: application/json' -i http://localhost:3000/server/del/ubuntu
```

### Recuperer tous les servers dans la plateforme flopbox

```bash
curl -X GET -H 'Accept: application/json' -H 'Content-Type: application/json' -i http://localhost:3000/server/all
```

### Lister les elements d'un serveur dans la plateforme flopbox

```bash
curl -X GET -H 'Accept: application/json' -H 'Content-Type: application/json' -i http://localhost:3000/server/list
```

### Acceder au repertoire parent dans la plateforme flopbox

```bash
curl -X GET -H 'Accept: application/json' -H 'Content-Type: application/json' -i http://localhost:3000/server/cdup
```

### Acceder à un repertoire dans la plateforme flopbox

```bash
curl -X GET -H 'Accept: application/json' -H 'Content-Type: application/json' -i http://localhost:3000/server/cwd/cdimage
```

### Créer un dossier dans la plateforme flopbox

```bash
curl -X GET -H 'Accept: application/json' -H 'Content-Type: application/json' -i http://localhost:3000/server/mkdir/test
```

### Modification d'un dossier dans la plateform flopbox

```bash
curl -X GET -H 'Content-Type: application/json' -H 'Accept: application/json' -i http://localhost:3000/server/rendir/test/ibrahima
```

### Renomer un dossier dans la plateform flopbox

```bash
curl -X GET -H 'Content-Type: application/json' -H 'Accept: application/json' -i http://localhost:3000/server/rmdir/ibrahima
```

### Couper la connection du serveur dans la plateform flopbox

```bash
curl -X GET -H 'Content-Type: application/json' -H 'Accept: application/json' -i http://localhost:3000/server/close
```

Pour tester essayer de lister le contenu d'un repertoire par exemple.

### Uploader un fichier dans le serveur sur la plateforme flopbox

```bash
curl -X POST -H 'Content-Type: application/json' -H 'Accept: application/json' -i http://localhost:3000/server/uploadFile --data '{"path": "/home/users/etudiant/Pictures/astronomy.jpg" }
```

Pour les fichiers text

```bash
curl -X POST -H 'Content-Type: application/json' -H 'Accept: application/json' -i http://localhost:3000/server/uploadFile --data '{"path": "/home/users/etudiant/Documents/commandeVPN.txt" }
```

### Telecharger un fichier

```bash
curl -X GET -H 'Content-Type: application/json' -H 'Accept: application/json' -i http://localhost:3000/server/dowloadFile/astronomy.jpg
```

## Architecture

{"path": "/home/users/etudiant/Documents/commandeVPN.txt" }

L'architecture mvc (modèle-vue-controller) a été utilisé.

Dans le dossier **_routes :_** Se trouve les routes des ressources utilisées pour les users et les serveurs.

Dans le dossier **_models :_** Se trouve les models des clients et des serveurs.

Dans le dossier **_controllers :_** Se trouve les controllers des serveurs ftp et des clients.

## Realisation

La documentation du code écrit a été fait.

Tout ce qui est transfert et reception dossier n'a pas été réalisé.
