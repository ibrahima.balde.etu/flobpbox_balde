var express = require('express');
var router = express.Router();
const controller =require('../controllers/serversControllers')
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/add', (req, res, next)=>{
  controller.createServer(req, res);
})

router.get('/del/:alias', (req, res, next)=>{
    controller.deleteServer(req, res);
  })

router.get('/all',  (req, res) => {
    controller.getAllServer(req, res);
})

router.get('/connect/:alias',  (req, res) => {
    controller.connectServer(req, res);
})

router.get('/update/:alias1/:alias',  (req, res) => {
    controller.updateServer(req, res);
})

router.get('/list',  (req, res) => {
    controller.listFolder(req, res);
})

router.get('/cwd/:directory',  (req, res) => {
    controller.changeDirectory(req, res);
})

router.get('/cdup',  (req, res) => {
    controller.getParent(req, res);
})

router.get('/mkdir/:directory',  (req, res) => {
    controller.makeDirectory(req, res);
})

router.put('/rendir/:directory/:newDirectory',  (req, res) => {
    controller.renameDirectory(req, res);
})

// Pour supprimer un serveur
router.get('/rmdir/:directory',  (req, res) => {
    controller.removeDirectory(req, res);
})

// Pour couper la connection du serveur
router.get('/close',  (req, res) => {
    controller.destroyServer(req, res);
})

// Pour stocker un fichier binaire ou texte
router.post('/uploadFile',  (req, res) => {
    controller.uploadFile(req, res);
})

// Pour stocker un fichier binaire ou texte
router.get('/dowloadFile/:file',  (req, res) => {
    controller.dowloadFile(req, res);
})

// Pour stocker un fichier binaire ou texte
router.post('/dowloadFolder',  (req, res) => {
    controller.dowloadFolder(req, res);
})


module.exports = router;
