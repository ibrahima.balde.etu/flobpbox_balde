const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    name: {type: String, required: true, unique:true},
    password: {type: String, required : true}
});


const dbConexion = require('../controllers/db');

const Users = dbConexion.model('User', userSchema, 'users');

module.exports.model = Users;