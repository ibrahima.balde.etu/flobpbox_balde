const mongoose = require('mongoose');

const serverSchema = new mongoose.Schema({
    alias : {type: String, required : true, unique:true},
    host: {type: String, required: true, unique:true},
    port: {type: String, required : true},
    user: {type: String, required : true },
    password : {type: String, required: true}
});


const dbConexion = require('../controllers/db');

const Servers = dbConexion.model('Servers', serverSchema, 'servers');

module.exports.model = Servers;