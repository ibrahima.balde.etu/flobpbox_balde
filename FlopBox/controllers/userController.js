const Users = require('../models/users').model; // on charge le modèle de l'utilisateur
var passwordHash = require('password-hash'); // module pour hasher le mot de passe
const jwt = require('jsonwebtoken') // module pour generer des tokens

/**
 * Fonction qui permet de créer un nouveau client
 * @param {*} req requete http
 * @param {*} res reponse http
 */

const createClient = (req, res)=>{
    let newUser = {...req.body}
    newUser["password"] = passwordHash.generate(newUser["password"]);
    Users.create(newUser)
         .then(user => res.status(200).json(user))
         .catch(err => res.status(400).json(err))
}

/**
 * Fonction qui permet de connecter un client elle genère un token pour lui dans le cookie.
 * Le token est valide 8000 * 1000 ms.
 * Elle verifie si le mot de passe est valide
 * @param {*} req requete http
 * @param {*} res reponse http
 * @returns code 200 si tout se passe bien et renvoie un objet json de l'utilisateur et 400 sinon
 */

const connect = (req, res)=>{
    let name = req.body.name;
    Users.findOne({name : name})
        .then(user => {
            if (passwordHash.verify(req.body.password, user["password"])) { // verification si le mot de passe est valide
                let jwtkey = "ibrahimaBAldekourawy";
                let jwtExpirySeconds = 8000; // en ms
                //console.log("yessssss");
                const token = jwt.sign({ name }, jwtkey, {
                    algorithm: "HS256",
                    expiresIn: jwtExpirySeconds,
                })
                res.cookie("token", token, { maxAge: jwtExpirySeconds * 1000 })
                return res.status(200).json(user) 
            }
            res.status(400).json({message: "mot de passe incorrect"})
        })
        .catch(err => res.status(400).json(err))
}




module.exports.connect = connect;
module.exports.createClient = createClient;