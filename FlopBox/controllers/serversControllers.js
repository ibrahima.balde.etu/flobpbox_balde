const Servers = require('../models/server').model;
const ftp =  require('./ftpControllers')
const ftpServer = new ftp.FTPController();

const jwt = require('jsonwebtoken')

let Client2 = require('ftp-client');

/**
 * Fonction qui permet de creer un serveur avec son alias. Elle renvoie le code 400 si l'utilisateur n'est pas connecté 
 * Verifie la validité du token c'est à dire si l'utilisateur est toujours connecté
 * @param {*} req requete http
 * @param {*} res reponse http
 * @returns code 200 si tout se passe bien et renvoie un objet json du server et 400 sinon
 */

const createServer = (req, res)=>{

    if (!userIsconnected(req)) {
        return res.status(400).json({message : "Session expirée"})
    }

    let server = {...req.body};
    Servers.create(server)
            .then(server => res.status(200).json(server))
            .catch(err => res.status(400).json(err));
}

/**
 * Fonction qui permet de supprimer un serveur. Elle renvoie le 400 si l'utilisateur n'est pas connecté 
 * Verifie la validité du token c'est à dire si l'utilisateur est toujours connecté
 * @param {*} req requete http
 * @param {*} res reponse http
 * @returns code 200 si tout se passe bien et renvoie un objet json du server et 400 sinon
 */

const deleteServer = (req, res)=>{
    if (!userIsconnected(req)) {
        return res.status(400).json({message : "Session expirée"})
    }
    
    let alias = req.params.alias;
    Servers.findOneAndRemove({alias : alias})
            .then(server => {
                if(server==null) return res.status(400).json({result : "Aucun serveru trouvé"})
                res.status(200).json(server)
            })
            .catch(err => res.status(400).json(err));
}

/**
 * Fonction qui permet d'afficher tous les serveurs. Elle renvoie le 400 si l'utilisateur n'est pas connecté 
 * Verifie la validité du token c'est à dire si l'utilisateur est toujours connecté
 * @param {*} req requete http
 * @param {*} res reponse http
 * @returns code 200 si tout se passe bien et renvoie un objet json du server et 400 sinon
 */

const getAllServer = (req, res)=>{

    if (!userIsconnected(req)) {
        return res.status(400).json({message : "Session expirée"})
    }

    Servers.find()
            .then(servers => res.status(200).json(servers))
            .catch(err => res.status(400).json(err));
}

/**
 * Fonction qui permet de connecter un serveur. Elle renvoie le 400 si l'utilisateur n'est pas connecté 
 * Verifie la validité du token c'est à dire si l'utilisateur est toujours connecté
 * @param {*} req requete http
 * @param {*} res reponse http
 * @returns code 200 si tout se passe bien et renvoie un objet json du server et 400 sinon
 */

const connectServer = (req, res)=>{
    console.log(userIsconnected(req));
    
    if (!userIsconnected(req)) {
        return res.status(400).json({message : "Session expirée"})
    }

    let alias = req.params.alias;
    Servers.findOne({alias : alias})
            .then(async (server) =>{
                await ftpServer.connect(server);
                setTimeout(()=>{
                    if (ftpServer.isConected()) {
                        return res.status(200).json(server)
                    } else {
                        res.status(400).json({err : "Verifiez que le serveur est correct"})
                    }
                }, 600)
                
            })
            .catch(err => res.status(400).json(err));
}

/**
 * Fonction qui permet de lister le contenu d'un serveur. Elle renvoie le 400 si l'utilisateur n'est pas connecté 
 * Verifie la validité du token c'est à dire si l'utilisateur est toujours connecté
 * @param {*} req requete http
 * @param {*} res reponse http
 * @returns code 200 si tout se passe bien et renvoie un objet json du server et 400 sinon
 */

const listFolder = (req, res)=>{

    if (!userIsconnected(req)) {
        return res.status(400).json({message : "Session expirée"})
    }

    if(!ftpServer.isConected()) return res.status(400).json({err: "Aucun serveur connecté"});
    
    ftpServer.list((err, list)=>{
        if(err) return res.status(400).json(err);
        res.status(200).json(list)
    })
}

/**
 * Fonction qui permet de changer de repertoire dans un serveur. Elle renvoie le 400 si l'utilisateur n'est pas connecté 
 * Verifie la validité du token c'est à dire si l'utilisateur est toujours connecté
 * @param {*} req requete http
 * @param {*} res reponse http
 * @returns code 200 si tout se passe bien et renvoie un objet json du server et 400 sinon
 */

const changeDirectory = (req, res)=>{

    if (!userIsconnected(req)) {
        return res.status(400).json({message : "Session expirée"})
    }

    if(!ftpServer.isConected()) return res.status(400).json({err: "Aucun serveur connecté"});
    
    let directory = req.params.directory;
    ftpServer.cwd(directory,(err, curent)=>{
        if(err) return res.status(400).json(err);
        res.status(200).json({result : curent})
    })
}

/**
 * Fonction qui permet de revenir dans un dossier parent dans un serveur. Elle renvoie le 400 si l'utilisateur n'est pas connecté 
 * Verifie la validité du token c'est à dire si l'utilisateur est toujours connecté
 * @param {*} req requete http
 * @param {*} res reponse http
 * @returns code 200 si tout se passe bien et renvoie un objet json du server et 400 sinon
 */
const getParent = (req, res)=>{

    if (!userIsconnected(req)) {
        return res.status(400).json({message : "Session expirée"})
    }

    if(!ftpServer.isConected()) return res.status(400).json({err: "Aucun serveur connecté"});

    ftpServer.cdup((err)=>{
        if(err) return res.status(400).json(err);
        res.status(200).json({result : "success"})
    })
}

/**
 * Fonction qui permet de créer un dossier dans un serveur. Elle renvoie le 400 si l'utilisateur n'est pas connecté 
 * Verifie la validité du token c'est à dire si l'utilisateur est toujours connecté
 * @param {*} req requete http
 * @param {*} res reponse http
 * @returns code 200 si tout se passe bien et renvoie un objet json du server et 400 sinon
 */

const makeDirectory = (req, res)=>{

    if (!userIsconnected(req)) {
        return res.status(400).json({message : "Session expirée"})
    }

    if(!ftpServer.isConected()) return res.status(400).json({err: "Aucun serveur connecté"});

    let directory = req.params.directory;

    ftpServer.mkdir(directory, (err)=>{
        if(err) return res.status(400).json(err);
        res.status(200).json({result : "success"})
    })
}


/**
 * Fonction qui permet de modifier un dossier dans un serveur. Elle renvoie le 400 si l'utilisateur n'est pas connecté 
 * Verifie la validité du token c'est à dire si l'utilisateur est toujours connecté
 * @param {*} req requete http
 * @param {*} res reponse http
 * @returns code 200 si tout se passe bien et renvoie un objet json du server et 400 sinon
 */

 const renameDirectory = (req, res)=>{

    if (!userIsconnected(req)) {
        return res.status(400).json({message : "Session expirée"})
    }

    if(!ftpServer.isConected()) return res.status(400).json({err: "Aucun serveur connecté"});

    let directory = req.params.directory;
    let newDirectory = req.params.newDirectory;

    ftpServer.rend(directory, newDirectory, (err)=>{
        if(err) return res.status(400).json(err);
        res.status(200).json({result : "success"})
    })
}

/**
 * Fonction qui permet de supprimer un dossier dans un serveur. Elle renvoie le 400 si l'utilisateur n'est pas connecté 
 * Verifie la validité du token c'est à dire si l'utilisateur est toujours connecté
 * @param {*} req requete http
 * @param {*} res reponse http
 * @returns code 200 si tout se passe bien et renvoie un objet json du server et 400 sinon
 */

const removeDirectory = (req, res)=>{

    if (!userIsconnected(req)) {
        return res.status(400).json({message : "Session expirée"})
    }

    if(!ftpServer.isConected()) return res.status(400).json({err: "Aucun serveur connecté"});

    let directory = req.params.directory;

    ftpServer.rmdir(directory, (err)=>{
        if(err) return res.status(400).json(err);
        res.status(200).json({result : "success"})
    })
}


/**
 * Fonction qui permet de fermer la connection d'un serveur. Elle renvoie le 400 si l'utilisateur n'est pas connecté 
 * Verifie la validité du token c'est à dire si l'utilisateur est toujours connecté
 * @param {*} req requete http
 * @param {*} res reponse http
 * @returns code 200 si tout se passe bien et renvoie un objet json du server et 400 sinon
 */

 const destroyServer = (req, res)=>{

    if (!userIsconnected(req)) {
        return res.status(400).json({message : "Session expirée"})
    }

    if(!ftpServer.isConected()) return res.status(400).json({err: "Aucun serveur connecté"});

    try {
        ftpServer.close();
        return res.status(200).json({result : "success"});
    } catch (error) {
        return res.status(400).json(error);
    }
    
}

/**
 * Fonction qui permet de mettre un ficher qu'il soit binaire ou pas dans un serveur . Elle renvoie le 400 si l'utilisateur n'est pas connecté 
 * Verifie la validité du token c'est à dire si l'utilisateur est toujours connecté
 * @param {*} req requete http
 * @param {*} res reponse http
 * @returns code 200 si tout se passe bien et renvoie un objet json du server et 400 sinon
 */

 const uploadFile = (req, res)=>{
    
    if (!userIsconnected(req)) {
        return res.status(400).json({message : "Session expirée"})
    }

    if(!ftpServer.isConected()) return res.status(400).json({err: "Aucun serveur connecté"});

    let directory = {...req.body}["path"] ;
    let path = directory.split('/');
    let name = path[path.length-1];
    ftpServer.put(directory, name,(err)=>{
        if(err) return res.status(400).json(err);
        res.status(200).json({result : "success"})
    })
}

/**
 * Fonction qui permet de telecharger un ficher dans un serveur. Elle renvoie le 400 si l'utilisateur n'est pas connecté 
 * Verifie la validité du token c'est à dire si l'utilisateur est toujours connecté
 * @param {*} req requete http
 * @param {*} res reponse http
 * @returns code 200 si tout se passe bien et renvoie un objet json du server et 400 sinon
 */

 const dowloadFile = (req, res)=>{
    
    if (!userIsconnected(req)) {
        return res.status(400).json({message : "Session expirée"})
    }

    if(!ftpServer.isConected()) return res.status(400).json({err: "Aucun serveur connecté"});
    let directory = req.params.file;
    
    
    ftpServer.get(directory,(err, stream)=>{
        if(err) return res.status(400).json(err);
        stream.pipe(res);
    })
}

const dowloadFolder = (req, res)=>{
    
    // if (!userIsconnected(req)) {
    //     return res.status(400).json({message : "Session expirée"})
    // }

    let config = 
    {
        host: 'localhost',
        port: 2121,
        user: 'balde',
        password: '12345'
    }

    client = new Client2(config);
    
    // if (!userIsconnected(req)) {
    //     return res.status(400).json({message : "Session expirée"})
    // }

    let directory = {...req.body}["path"] ;
    console.log(directory);
    let path = directory.split('/');
    let name = path[path.length-1];
    
    client.download(directory, name, {
        overwrite: 'all'
    }, function (result) {
        console.log(result);
        res.status(200).json({result : "success"});
    });
    

}



/**
 * Verifie la validité du token c'est à dire si l'utilisateur est toujours connecté
 * @param {*} req requete http
 * @param {*} res reponse http
 * @returns code 200 si tout se passe bien et renvoie un objet json du server et 400 sinon
 */

const userIsconnected = (req)=>{
    const token = req.cookies.token
    let jwtkey = "ibrahimaBAldekourawy";
    try {
      let payload = jwt.verify(token, jwtkey)
      //console.log(jwt.verify(token, jwtkey));
      return true;
    } catch (error) {
      return false;
    }
  }


module.exports.dowloadFolder = dowloadFolder;
module.exports.dowloadFile = dowloadFile;
module.exports.uploadFile = uploadFile;
module.exports.destroyServer = destroyServer;
module.exports.removeDirectory = removeDirectory;
module.exports.renameDirectory = renameDirectory;
module.exports.makeDirectory = makeDirectory;
module.exports.getParent = getParent;
module.exports.changeDirectory = changeDirectory;
module.exports.listFolder = listFolder;
module.exports.connectServer = connectServer;
module.exports.getAllServer = getAllServer;
module.exports.deleteServer = deleteServer;
module.exports.createServer = createServer;