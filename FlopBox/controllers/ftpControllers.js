let Client = require('ftp');

/**
 * Classe qui perment de gerer un serveur ftp
 */

class FTPController {
    constructor(){
        this.connected = false; // si le serveur est connecté

        this.client = new Client(); // on crée un nouveau client

        this.client.on('ready', ()=>{
            this.connected = true;
        })
        this.client.on('error', ()=>{
            this.connected = false;
        })
    }

    /**
     * Permet de verifier si le serveur est connecté
     * @returns boolean selon que le serveur est connecté ou pas
     */

    isConected() { return this.connected}

    /**
     * Permet de connecter un serveur
     * @param {*} server le serveur à connecter
     */
    connect(server){
        this.client.connect(server);
    }

    /**
     * Permet de lister le contenu d'un serveur
     * @param {*} callback 
     */

    list(callback){
        this.client.list((err, list)=>{
            if(err) return callback(err, null);
            callback(null, list)
        })
    }

    /**
     * Permet de changer de repertoire dans un serveur
     * @param {*} path Le chemin du repertoire
     * @param {*} callback 
     */

    cwd(path, callback){
        this.client.cwd(path, (err, curent)=>{
            if(err) return callback(err, null);
            callback(null, curent)
        })
    }

    /**
     * Permet de revenir dans le parent d'un noeud dans un serveur
     * @param {*} callback 
     */
    cdup(callback){
        this.client.cdup((err)=>{
            if(err) return callback(err);
            callback(null)
        })
    }

    /**
     * Permet de créer un dossier dans un repertoire
     * @param {*} path 
     * @param {*} callback 
     */

    mkdir(path ,callback){
        this.client.mkdir(path, (err)=>{
            if(err) return callback(err);
            callback(null)
        })
    }

    rmdir(path ,callback){
        this.client.rmdir(path, (err)=>{
            if(err) return callback(err);
            callback(null)
        })
    }

    /**
     * Permet de modifier un dossier
     * @param {*} oldPath 
     * @param {*} newPath 
     * @param {*} callback 
     */

     rend(oldPath , newPath ,callback){
        this.client.rename(oldPath, newPath, (err)=>{
            if(err) return callback(err);
            callback(null)
        })
    }

    close(callback){
        this.client.destroy();
        this.connected = false;
    }

    /**
     * Permet de modifier un dossier
     * @param {*} input 
     * @param {*} path 
     * @param {*} callback 
     */

     put(input , path ,callback){
        this.client.put(input, path, (err)=>{
            if(err) return callback(err);
            callback(null)
        })
    }

    /**
     * Permet de modifier un dossier
     * @param {*} input 
     * @param {*} path 
     * @param {*} callback 
     */

     get(input, callback){
        this.client.get(input, (err, stream)=>{
            if(err) return callback(err, null);
            callback(null, stream);
        })
    }


}

module.exports.FTPController = FTPController;